defmodule QueryElf.Plugins.AutomaticFiltersTest do
  use ExUnit.Case, async: true

  import Ecto.Query

  defmodule UnixTime do
    use Ecto.Type

    def type, do: :integer
    def cast(value) when is_integer(value), do: {:ok, DateTime.from_unix!(value)}
    def cast(value) when is_struct(value, DateTime), do: {:ok, value}
    def cast(_value), do: :error
    def load(value) when is_binary(value), do: {:ok, DateTime.from_unix!(value)}
    def dump(value) when is_struct(value, DateTime), do: {:ok, DateTime.to_unix(value)}
    def dump(_value), do: :error
  end

  defmodule TestSchema do
    use Ecto.Schema

    embedded_schema do
      field :my_int, :integer
      field :my_string, :string
      field :my_bool, :boolean
      field :my_date, :date
      field :my_enum, Ecto.Enum, values: [foo: 1, bar: 2, baz: 3]
      field :my_array, {:array, Ecto.Enum}, values: [:foo, :bar, :baz]
      field :my_datetime, UnixTime
    end
  end

  defmodule QB do
    use QueryElf,
      schema: TestSchema,
      plugins: [
        {
          QueryElf.Plugins.AutomaticFilters,
          fields: ~w[id my_int my_string my_bool my_date my_enum my_array my_datetime]a
        }
      ]
  end

  test "defines automatic filters for id fields" do
    assert_equal_queries(QB.build_query(id: 1), where(TestSchema, id: ^1))
    assert_equal_queries(QB.build_query(id__neq: 1), where(TestSchema, [s], s.id != ^1))
    assert_equal_queries(QB.build_query(id__in: [1, 2]), where(TestSchema, [s], s.id in ^[1, 2]))
  end

  test "defines automatic filters for numeric fields" do
    assert_equal_queries(QB.build_query(my_int: 1), where(TestSchema, my_int: ^1))
    assert_equal_queries(QB.build_query(my_int__neq: 1), where(TestSchema, [s], s.my_int != ^1))

    assert_equal_queries(QB.build_query(my_int__gt: 1), where(TestSchema, [s], s.my_int > ^1))
    assert_equal_queries(QB.build_query(my_int__lt: 1), where(TestSchema, [s], s.my_int < ^1))
    assert_equal_queries(QB.build_query(my_int__gte: 1), where(TestSchema, [s], s.my_int >= ^1))
    assert_equal_queries(QB.build_query(my_int__lte: 1), where(TestSchema, [s], s.my_int <= ^1))

    assert_equal_queries(
      QB.build_query(my_int__in: [1, 2]),
      where(TestSchema, [s], s.my_int in ^[1, 2])
    )

    assert_equal_queries(
      QB.build_query(my_int__is_empty: true),
      where(TestSchema, [s], is_nil(s.my_int))
    )

    assert_equal_queries(
      QB.build_query(my_int__is_empty: false),
      where(TestSchema, [s], not is_nil(s.my_int))
    )
  end

  test "defines automatic filters for string fields" do
    assert_equal_queries(QB.build_query(my_string: "a"), where(TestSchema, my_string: ^"a"))

    assert_equal_queries(
      QB.build_query(my_string__neq: "a"),
      where(TestSchema, [s], s.my_string != ^"a")
    )

    assert_equal_queries(
      QB.build_query(my_string__contains: "a"),
      where(TestSchema, [s], like(s.my_string, ^"%a%"))
    )

    assert_equal_queries(
      QB.build_query(my_string__starts_with: "a"),
      where(TestSchema, [s], like(s.my_string, ^"a%"))
    )

    assert_equal_queries(
      QB.build_query(my_string__ends_with: "a"),
      where(TestSchema, [s], like(s.my_string, ^"%a"))
    )

    assert_equal_queries(
      QB.build_query(my_string__in: ["a", "b"]),
      where(TestSchema, [s], s.my_string in ^["a", "b"])
    )

    assert_equal_queries(
      QB.build_query(my_string__is_empty: true),
      where(TestSchema, [s], is_nil(s.my_string))
    )

    assert_equal_queries(
      QB.build_query(my_string__is_empty: false),
      where(TestSchema, [s], not is_nil(s.my_string))
    )
  end

  test "defines automatic filters for boolean fields" do
    assert_equal_queries(QB.build_query(my_bool: true), where(TestSchema, my_bool: ^true))
  end

  test "defines automatic filters for date fields" do
    assert_equal_queries(
      QB.build_query(my_date: ~D[2019-01-01]),
      where(TestSchema, my_date: ^~D[2019-01-01])
    )

    assert_equal_queries(
      QB.build_query(my_date__neq: ~D[2019-01-01]),
      where(TestSchema, [s], s.my_date != ^~D[2019-01-01])
    )

    assert_equal_queries(
      QB.build_query(my_date__after: ~D[2019-01-01]),
      where(TestSchema, [s], s.my_date > ^~D[2019-01-01])
    )

    assert_equal_queries(
      QB.build_query(my_date__before: ~D[2019-01-01]),
      where(TestSchema, [s], s.my_date < ^~D[2019-01-01])
    )

    assert_equal_queries(
      QB.build_query(my_date__in: [~D[2019-01-01], ~D[2019-01-04]]),
      where(TestSchema, [s], s.my_date in ^[~D[2019-01-01], ~D[2019-01-04]])
    )

    assert_equal_queries(
      QB.build_query(my_date__is_empty: true),
      where(TestSchema, [s], is_nil(s.my_date))
    )

    assert_equal_queries(
      QB.build_query(my_date__is_empty: false),
      where(TestSchema, [s], not is_nil(s.my_date))
    )
  end

  test "defines automatic filters for enum fields" do
    assert_equal_queries(
      QB.build_query(my_enum: :foo),
      where(TestSchema, my_enum: ^:foo)
    )

    assert_equal_queries(
      QB.build_query(my_enum__neq: :foo),
      where(TestSchema, [s], s.my_enum != ^:foo)
    )

    assert_equal_queries(
      QB.build_query(my_enum__in: [:foo, :bar]),
      where(TestSchema, [s], s.my_enum in ^[:foo, :bar])
    )

    assert_equal_queries(
      QB.build_query(my_enum__not_in: [:bar, :baz]),
      where(TestSchema, [s], s.my_enum not in ^[:bar, :baz])
    )
  end

  test "defines automatic filters for array fields" do
    assert_equal_queries(
      QB.build_query(my_array: [:foo, :bar]),
      where(TestSchema, [s], s.my_array == ^[:foo, :bar])
    )

    assert_equal_queries(
      QB.build_query(my_array__neq: [:baz]),
      where(TestSchema, [s], s.my_array != ^[:baz])
    )

    assert_equal_queries(
      QB.build_query(my_array__includes: :foo),
      where(TestSchema, [s], ^:foo in s.my_array)
    )

    assert_equal_queries(
      QB.build_query(my_array__does_not_includes: :foo),
      where(TestSchema, [s], ^:foo not in s.my_array)
    )
  end

  test "defines automatic filters for custom type fields" do
    now = DateTime.utc_now()

    assert_equal_queries(
      QB.build_query(my_datetime: now),
      where(TestSchema, my_datetime: ^now)
    )

    assert_equal_queries(
      QB.build_query(my_datetime__gt: DateTime.add(now, 5, :day)),
      where(TestSchema, [s], s.my_datetime > ^DateTime.add(now, 5, :day))
    )

    assert_equal_queries(
      QB.build_query(my_datetime__lte: 1_672_531_200),
      where(TestSchema, [s], s.my_datetime <= ^1_672_531_200)
    )
  end

  defp assert_equal_queries(q1, q2) do
    assert inspect(q1) == inspect(q2)
  end
end
